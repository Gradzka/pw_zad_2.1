// Critical_section.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>  
#include <stdio.h>  
#include <stdlib.h> 
#include "iostream"
#include <omp.h>  

#define n 10
#define threadCount 4 //ilosc watkow
int sum_of_elem_in_the_table;
int table[n];

CRITICAL_SECTION CriticalSection; // deklaracja sekcji krytycznej

DWORD WINAPI ThreadProc(LPVOID lpParameter)
{
	int equal_chunks; //ok
	n % threadCount >= threadCount / 2 ? equal_chunks = ((n / threadCount) + 1) : equal_chunks = n / threadCount;
	int first = ((int)lpParameter) * equal_chunks;
	int last;
	((int)lpParameter == threadCount - 1) ? last = n : last = first + equal_chunks;

	int temp_sum = 0;
	for (int i = first; i < last; i++)
	{
		temp_sum += table[i];
		//printf("Jestem watkiem nr %d dodaje %d \n",(int)lpParameter,i);
	}
	EnterCriticalSection(&CriticalSection); //wejscie do sekcji krytycznej
	sum_of_elem_in_the_table += temp_sum;
	LeaveCriticalSection(&CriticalSection); //opuszczenie sekcji krytycznej
	return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	DWORD dwThreadId;
	double start, end,result;
	HANDLE hThread[threadCount];
	for (int i = 0; i < n; i++)
		table[i] = i;
	for (int i = 0; i < n; i++)
		std::cout<< i<<"\t";

	InitializeCriticalSection(&CriticalSection);
	start = omp_get_wtime();
	volatile int k = 0;
	for (int i = 0; i<threadCount; i++)
	{
		hThread[i] = CreateThread(NULL, 0, ThreadProc, (PVOID)k, 0, &dwThreadId);
		k++;
		if (hThread[i] == NULL) ExitProcess(3);
	}
	WaitForMultipleObjects(threadCount, hThread, TRUE, INFINITE);
	end = omp_get_wtime();
	for (int i = 0; i < threadCount; i++)
	{
		CloseHandle(hThread[i]);
	}

	DeleteCriticalSection(&CriticalSection);
	printf("Suma elementow tablicy: %d\n", sum_of_elem_in_the_table);
	result = end - start;
	printf("Czas wykonania w sekundach: %f\n", result);
	system("pause");
	return 0;
}

